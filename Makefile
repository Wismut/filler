#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mivanov <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/05 15:25:00 by mivanov           #+#    #+#              #
#    Updated: 2017/03/06 13:04:56 by mivanov          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = filler

G = gcc

FLAG = -Wall -Wextra -Werror

LIB = libft.a

SRC = main.c read_from_console.c

OBJ = $(SRC:.c=.o)

LIBFTDIR = libft

all: $(NAME)

$(NAME) : $(OBJ) $(LIB)
			@$(G) $(FLAG) $(OBJ) -o $(NAME) libft/$(LIB) -o $(NAME)
%.o: %.c
			@$(G) $(FLAG) -c $<

$(LIB) :
		@make -C libft/

cleanlib:
		@make -C libft/ clean

fcleanlib:
		@make -C libft/ fclean

clean: cleanlib
			@rm -f $(OBJ)

fclean: clean fcleanlib
			@rm -f $(NAME)

re: fclean all