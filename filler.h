#ifndef FILLER_H
# define FILLER_H

# include "libft/libft.h"
# include "libft/get_next_line.h"

typedef struct		s_lines
{
	char			*line;
	struct s_lines	*next;
}					t_lines;

void				read_from_console(void);

#endif
