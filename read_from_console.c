#include "filler.h"
#include <stdio.h>

void	read_from_console(void)
{
	char	*line;
	int 	fd;
	t_lines	*lines;

	lines = NULL;
	fd = open("/nfs/2016/m/mivanov/ClionProjects/Filler/123", O_WRONLY);
	while (get_next_line(0, &line) > 0)
	{
		ft_putstr("8 2\n");
		write(fd, line, ft_strlen(line));
		write(fd, "\n", 1);
	}
	close(fd);
}

char	**get_memory_for_map(char *str)
{
	char	**array;
	char 	**res;
	int 	x;
	int 	y;
	int 	k;

	array = ft_strsplit(str, ' ');
	x = ft_atoi(array[1]);
	y = ft_atoi(array[2]);
	k = y;
	y = 0;
	res = (char **)malloc(sizeof(char *) * (x + 1));
	while (y < k)
		res[y++] = (char *)malloc(k);
	res[y] = NULL;
	return (res);
}
